FROM docker.io/library/python:3.9-slim
ENV PYTHONUNBUFFERED 1
ADD   . /moto/
WORKDIR /moto/
RUN  pip3 --no-cache-dir install --upgrade pip setuptools && \
     pip3 --no-cache-dir install ".[server]" && \
     cd / && \
     echo "Cleaning up local source directory to avoid confusion" && \
     rm -rf /moto
WORKDIR /usr/local/bin

ENTRYPOINT ["/usr/local/bin/moto_server", "-H", "0.0.0.0"]

EXPOSE 5000
