from .responses import RedshiftResponse

# language=RegExp
url_bases = [r"https?://redshift\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": RedshiftResponse.dispatch}
