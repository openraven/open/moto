from .responses import CognitoIdentityResponse

# language=RegExp
url_bases = [r"https?://cognito-identity\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {"/$": CognitoIdentityResponse.dispatch}
