from .responses import SageMakerResponse

# language=RegExp
url_bases = [
    r"https?://api.sagemaker\.(.+)\.amazonaws.com",
]

# language=RegExp
url_paths = {
    "/$": SageMakerResponse.dispatch,
}
