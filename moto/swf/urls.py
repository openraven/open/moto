from .responses import SWFResponse

# language=RegExp
url_bases = [r"https?://swf\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": SWFResponse.dispatch}
