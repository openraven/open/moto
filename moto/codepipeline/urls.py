from .responses import CodePipelineResponse

# language=RegExp
url_bases = [r"https?://codepipeline\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": CodePipelineResponse.dispatch}
