from .responses import SupportResponse

# language=RegExp
url_bases = [r"https?://support\.(.+)\.amazonaws\.com"]


# language=RegExp
url_paths = {
    "/$": SupportResponse.dispatch,
}
