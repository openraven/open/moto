from .responses import MediaPackageResponse

# language=RegExp
url_bases = [
    r"https?://mediapackage\.(.+)\.amazonaws.com",
]


response = MediaPackageResponse()


# language=RegExp
url_paths = {
    "/channels": response.dispatch,
    "/channels/(?P<channelid>[^/.]+)": response.dispatch,
    "/origin_endpoints": response.dispatch,
    "/origin_endpoints/(?P<id>[^/.]+)": response.dispatch,
}
