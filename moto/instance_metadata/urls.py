from .responses import InstanceMetadataResponse

# language=RegExp
url_bases = ["http://169.254.169.254"]

instance_metadata = InstanceMetadataResponse()

# language=RegExp
url_paths = {"/(?P<path>.+)": instance_metadata.metadata_response}
