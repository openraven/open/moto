from .responses import APIGatewayResponse

response = APIGatewayResponse()

# language=RegExp
url_bases = [r"https?://apigateway\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {
    "/restapis$": response.restapis,
    "/restapis/(?P<function_id>[^/]+)/?$": response.restapis_individual,
    "/restapis/(?P<function_id>[^/]+)/resources$": response.resources,
    "/restapis/(?P<function_id>[^/]+)/authorizers$": response.restapis_authorizers,
    "/restapis/(?P<function_id>[^/]+)/authorizers/(?P<authorizer_id>[^/]+)/?$": response.authorizers,
    "/restapis/(?P<function_id>[^/]+)/stages$": response.restapis_stages,
    "/tags/arn:aws:apigateway:(?P<region_name>[^/]+)::/restapis/(?P<function_id>[^/]+)/stages/(?P<stage_name>[^/]+)/?$": response.restapis_stages_tags,
    "/restapis/(?P<function_id>[^/]+)/stages/(?P<stage_name>[^/]+)/?$": response.stages,
    "/restapis/(?P<function_id>[^/]+)/deployments$": response.deployments,
    "/restapis/(?P<function_id>[^/]+)/deployments/(?P<deployment_id>[^/]+)/?$": response.individual_deployment,
    "/restapis/(?P<function_id>[^/]+)/resources/(?P<resource_id>[^/]+)/?$": response.resource_individual,
    "/restapis/(?P<function_id>[^/]+)/resources/(?P<resource_id>[^/]+)/methods/(?P<method_name>[^/]+)/?$": response.resource_methods,
    r"/restapis/(?P<function_id>[^/]+)/resources/(?P<resource_id>[^/]+)/methods/(?P<method_name>[^/]+)/responses/(?P<status_code>\d+)$": response.resource_method_responses,
    "/restapis/(?P<function_id>[^/]+)/resources/(?P<resource_id>[^/]+)/methods/(?P<method_name>[^/]+)/integration/?$": response.integrations,
    r"/restapis/(?P<function_id>[^/]+)/resources/(?P<resource_id>[^/]+)/methods/(?P<method_name>[^/]+)/integration/responses/(?P<status_code>\d+)/?$": response.integration_responses,
    "/apikeys$": response.apikeys,
    "/apikeys/(?P<apikey>[^/]+)": response.apikey_individual,
    "/usageplans$": response.usage_plans,
    "/domainnames$": response.domain_names,
    "/restapis/(?P<function_id>[^/]+)/models$": response.models,
    "/restapis/(?P<function_id>[^/]+)/models/(?P<model_name>[^/]+)/?$": response.model_induvidual,
    "/domainnames/(?P<domain_name>[^/]+)/?$": response.domain_name_induvidual,
    "/domainnames/(?P<domain_name>[^/]+)/basepathmappings$": response.base_path_mappings,
    "/domainnames/(?P<domain_name>[^/]+)/basepathmappings/(?P<base_path_mapping>[^/]+)$": response.base_path_mapping_individual,
    "/usageplans/(?P<usage_plan_id>[^/]+)/?$": response.usage_plan_individual,
    "/usageplans/(?P<usage_plan_id>[^/]+)/keys$": response.usage_plan_keys,
    "/usageplans/(?P<usage_plan_id>[^/]+)/keys/(?P<api_key_id>[^/]+)/?$": response.usage_plan_key_individual,
    "/restapis/(?P<function_id>[^/]+)/requestvalidators$": response.request_validators,
    "/restapis/(?P<api_id>[^/]+)/requestvalidators/(?P<validator_id>[^/]+)/?$": response.request_validator_individual,
}
