from .responses import AutoScalingResponse

# language=RegExp
url_bases = [r"https?://autoscaling\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": AutoScalingResponse.dispatch}
