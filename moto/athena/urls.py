from .responses import AthenaResponse

# language=RegExp
url_bases = [r"https?://athena\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": AthenaResponse.dispatch}
