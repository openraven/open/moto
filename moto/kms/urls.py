from .responses import KmsResponse

# language=RegExp
url_bases = [r"https?://kms\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": KmsResponse.dispatch}
