from .responses import XRayResponse

# language=RegExp
url_bases = [r"https?://xray\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {
    "/TelemetryRecords$": XRayResponse.dispatch,
    "/TraceSegments$": XRayResponse.dispatch,
    "/Traces$": XRayResponse.dispatch,
    "/ServiceGraph$": XRayResponse.dispatch,
    "/TraceGraph$": XRayResponse.dispatch,
    "/TraceSummaries$": XRayResponse.dispatch,
}
