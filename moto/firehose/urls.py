"""Firehose base URL and path."""
from .responses import FirehoseResponse


url_bases = [r"https?://firehose\.(.+)\.amazonaws\.com"]
url_paths = {"/$": FirehoseResponse.dispatch}
