from .models import _iam_backend

# fucking flake8 is too stupid to see this in use
from .models import iam_backends_by_account_id  # noqa

iam_backends = {"global": _iam_backend}
mock_iam = _iam_backend.decorator
mock_iam_deprecated = _iam_backend.deprecated_decorator
