"""
This implementation is NOT complete, there are many things to improve.
The following is a list of the most important missing features and inaccuracies.

TODO add support for more principals, apart from IAM users and assumed IAM roles
TODO add support for the Resource and Condition parts of IAM policies
TODO add support and create tests for all services in moto (for example, API Gateway is probably not supported currently)
TODO implement service specific error messages (currently, EC2 and S3 are supported separately, everything else defaults to the errors IAM returns)
TODO include information about the action's resource in error messages (once the Resource element in IAM policies is supported)
TODO check all other actions that are performed by the action called by the user
  (for example, autoscaling:CreateAutoScalingGroup requires permission for iam:CreateServiceLinkedRole too
  - see https://docs.aws.amazon.com/autoscaling/ec2/userguide/control-access-using-iam.html)
TODO add support for resource-based policies

"""

import json
import logging
import re
from abc import abstractmethod, ABCMeta
from datetime import datetime
from enum import Enum
import typing as t

from botocore.auth import SigV4Auth, S3SigV4Auth
from botocore.awsrequest import AWSRequest
from botocore.credentials import Credentials

from moto.core.exceptions import (
    SignatureDoesNotMatchError,
    AccessDeniedError,
    InvalidClientTokenIdError,
    AuthFailureError,
)
from moto.s3.exceptions import (
    BucketAccessDeniedError,
    S3AccessDeniedError,
    BucketInvalidTokenError,
    S3InvalidTokenError,
    S3InvalidAccessKeyIdError,
    BucketInvalidAccessKeyIdError,
    BucketSignatureDoesNotMatchError,
    S3SignatureDoesNotMatchError,
)
from moto.sts import sts_backend
from .exceptions import IAMNotFoundException
from .models import iam_backends_by_account_id, Policy
from moto.utilities.utils import ACCESS_KEY_ID_REGEX

log = logging.getLogger(__name__)


def capture_request_credentials(iam_backend, access_key_id: str, headers):
    """
    Packages up the provided credentials from the HTTP request so they can be validated.
    :type iam_backend: moto.iam.models.IAMBackend
    :param str access_key_id:
    :param dict headers:
    """
    if access_key_id.startswith("AKIA") or "X-Amz-Security-Token" not in headers:
        return IAMUserAccessKey(iam_backend, access_key_id, headers)
    else:
        # TODO: currently this uses one sts_backend for all AccountIds
        return AssumedRoleAccessKey(iam_backend, access_key_id, headers)


class IAMUserAccessKey(object):
    __slots__ = (
        "iam_backend",
        "account_id",
        "_owner_user_arn",
        "_access_key_id",
        "_secret_access_key",
    )

    def __init__(self, iam_backend, access_key_id: str, headers):
        """
        :type iam_backend: moto.iam.models.IAMBackend
        :param str access_key_id:
        :param dict headers:
        :raises CreateAccessKeyFailure:
        """
        self.iam_backend = iam_backend
        access_key = iam_backend.access_keys.get(access_key_id)
        if not access_key:
            raise CreateAccessKeyFailure(reason="InvalidId")
        if "X-Amz-Security-Token" in headers:
            log.error(
                "AccessKey %s should not be used with Security Token", access_key_id
            )
            raise CreateAccessKeyFailure(reason="InvalidToken")
        self.account_id = access_key.account_id
        self._owner_user_arn = access_key.user_arn
        self._access_key_id = access_key_id
        self._secret_access_key = access_key.secret_access_key

    @property
    def arn(self):
        return self._owner_user_arn

    def create_credentials(self):
        return Credentials(self._access_key_id, self._secret_access_key)

    def collect_policies(self) -> t.List[t.Dict[str, t.Any]]:
        return self.collect_user_policies(self.iam_backend, self._owner_user_arn)

    @staticmethod
    def collect_user_policies(iam_backend, user_arn) -> t.List[t.Dict[str, t.Any]]:
        if user_arn.endswith(":root"):
            # we need to return *some* policy or the evaluation loop defaults to Deny
            return [{"policy_document": IAMPolicy.ROOT_CREDENTIAL_POLICY}]
        user_policies = []
        inline_policy_names = iam_backend.list_user_policies(user_arn)
        for inline_policy_name in inline_policy_names:
            inline_policy = iam_backend.get_user_policy(user_arn, inline_policy_name)
            user_policies.append(inline_policy)

        attached_policies, _ = iam_backend.list_attached_user_policies(user_arn)
        user_policies += attached_policies

        user_groups = iam_backend.get_groups_for_user(user_arn)
        for user_group in user_groups:
            inline_group_policy_names = iam_backend.list_group_policies(user_group.name)
            for inline_group_policy_name in inline_group_policy_names:
                inline_user_group_policy = iam_backend.get_group_policy(
                    user_group.name, inline_group_policy_name
                )
                user_policies.append(inline_user_group_policy)

            attached_group_policies, _ = iam_backend.list_attached_group_policies(
                user_group.name
            )
            user_policies += attached_group_policies

        return user_policies

    def __str__(self):
        return self.arn

    def __repr__(self):
        return "%s{%s}" % (
            self.__class__.__name__,
            {k: getattr(self, k) for k in self.__slots__},
        )


class AssumedRoleAccessKey(object):
    """This is only used in the Access Control system, and is different from :class:`AssumedRole`"""

    __slots__ = (
        "account_id",
        "iam_backend",
        "_owner_user_arn",
        "_owner_role_name",
        "_access_key_id",
        "_secret_access_key",
        "_session_token",
        "_session_name",
    )
    account_id: str

    def __init__(self, iam_backend, access_key_id, headers):
        """
        :type iam_backend: moto.iam.models.IAMBackend
        :param str access_key_id:
        :param dict headers:
        :raises CreateAccessKeyFailure:
        """
        self.iam_backend = iam_backend
        self.account_id = iam_backend.account_id
        self._owner_user_arn = None
        token = sts_backend.session_tokens.get(access_key_id)
        if token:
            self._access_key_id = access_key_id
            self._secret_access_key = token.secret_access_key
            self._session_token = token.session_token
            if token.assumed_role:
                self._owner_role_name = self._extract_role_from_assumed_role(
                    token.assumed_role.role_arn
                )
            elif token.user:
                self._owner_user_arn = token.user.arn
            elif token.is_root:
                self._owner_user_arn = iam_backend.root_arn
            else:
                log.error("What kind of Token is %s", repr(token))
                raise CreateAccessKeyFailure(reason="InvalidToken")
            if headers["X-Amz-Security-Token"] != self._session_token:
                raise CreateAccessKeyFailure(reason="InvalidToken")
            return
        for assumed_role in sts_backend.assumed_roles:
            if assumed_role.access_key_id == access_key_id:
                self._access_key_id = access_key_id
                self._secret_access_key = assumed_role.secret_access_key
                self._session_token = assumed_role.session_token
                self._session_name = assumed_role.session_name
                self._owner_role_name = self._extract_role_from_assumed_role(
                    assumed_role.role_arn
                )
                if headers["X-Amz-Security-Token"] != self._session_token:
                    raise CreateAccessKeyFailure(reason="InvalidToken")
                return
        raise CreateAccessKeyFailure(reason="InvalidId")

    @property
    def arn(self):
        return (
            "arn:aws:sts::{account_id}:assumed-role/{role_name}/{session_name}".format(
                account_id=self.account_id,
                role_name=self._owner_role_name,
                session_name=self._session_name,
            )
        )

    def create_credentials(self):
        return Credentials(
            self._access_key_id, self._secret_access_key, self._session_token
        )

    def collect_policies(self):
        if self._owner_user_arn:
            return IAMUserAccessKey.collect_user_policies(
                self.iam_backend, self._owner_user_arn
            )
        return self.collect_role_policies()

    def collect_role_policies(self):
        role_policies = []
        inline_policy_names = self.iam_backend.list_role_policies(self._owner_role_name)
        for inline_policy_name in inline_policy_names:
            _, inline_policy = self.iam_backend.get_role_policy(
                self._owner_role_name, inline_policy_name
            )
            role_policies.append(inline_policy)

        attached_policies, _ = self.iam_backend.list_attached_role_policies(
            self._owner_role_name
        )
        role_policies += attached_policies

        return role_policies

    @staticmethod
    def _extract_role_from_assumed_role(assumed_role_arn):
        ma = re.search(r"arn:aws:iam::(\d+):role/([^/]+)", assumed_role_arn)
        if not ma:
            log.error('Malformed owner role arn: "%s"', assumed_role_arn)
            raise CreateAccessKeyFailure(reason="InvalidToken")
        return ma.group(2)

    def __str__(self):
        return self.arn

    def __repr__(self):
        return "%s{%s}" % (
            self.__class__.__name__,
            {k: getattr(self, k) for k in self.__slots__},
        )


class CreateAccessKeyFailure(Exception):
    def __init__(self, reason, *args):
        super(CreateAccessKeyFailure, self).__init__(*args)
        self.reason = reason


class IAMRequestBase(object, metaclass=ABCMeta):
    def __init__(
        self,
        account_id: str,
        method: str,
        path: str,
        data: t.OrderedDict,
        headers: t.Dict[str, t.Any],
    ):
        log.log(
            1,
            "Creating {class_name} with account_id={account_id} method={method}, path={path}, data={data}, headers={headers!r}".format(
                class_name=self.__class__.__name__,
                account_id=account_id,
                method=method,
                path=path,
                data=data,
                headers=headers,
            ),
        )
        self.account_id = account_id
        iam_backend = iam_backends_by_account_id(account_id)
        self._method = method
        self._path = path
        self._data = data
        self._headers = headers
        auth_header = self._headers["Authorization"]
        ma = ACCESS_KEY_ID_REGEX.match(auth_header)
        if not ma:
            log.error("Unable to match Authorization: %s", auth_header)
            self._raise_invalid_access_key("malformed Authorization header")
        access_key_id = ma.group("access_key_id")
        self._region = ma.group("region")
        self._service = ma.group("service")
        sign_date = ma.group("yyyymmdd")
        date_fmt = "%Y%m%d"
        utc_date = datetime.utcnow().strftime(date_fmt)
        if sign_date != utc_date:
            log.info(
                "Signature Date seems suspicious: theirs=%s now=%s", sign_date, utc_date
            )
            # self._raise_invalid_access_key("wrong date in signature")

        if "X-Amz-Target" in self._headers:
            amz_target: str = self._headers["X-Amz-Target"]
            # example: X-Amz-Target: AWSOrganizationsV20161128.CreateOrganization
            ma = re.match(r"^.*?\.([^.]+)$", amz_target)
            if not ma:
                self._raise_invalid_access_key("malformed X-Amz-Target header")
            target_action = ma.group(1)
            self._action = "{}:{}".format(self._service, target_action)
        else:
            self._action = (
                self._service
                + ":"
                + (
                    self._data["Action"][0]
                    if isinstance(self._data["Action"], list)
                    else self._data["Action"]
                )
            )
        try:
            last_used = iam_backend.get_access_key_last_used(access_key_id)
            user_arn = last_used["user_arn"]
            log.debug(
                "Admitting account_id=%s user=%s by AccessKey", account_id, user_arn
            )
        except IAMNotFoundException:
            pass
        try:
            self._access_key = capture_request_credentials(
                iam_backend, access_key_id=access_key_id, headers=headers
            )
        except CreateAccessKeyFailure as e:
            log.exception(
                "failure while capturing AccessKey[%s] for account_id=%s",
                access_key_id,
                account_id,
            )
            self._raise_invalid_access_key(e.reason)

    def check_signature(self):
        auth_header = self._headers["Authorization"]
        ma = ACCESS_KEY_ID_REGEX.match(auth_header)
        if not ma:
            log.error("Malformed Authorization header: %s", auth_header)
            self._raise_invalid_access_key("Malformed Authorization header")
        original_signature = ma.group("signature")
        calculated_signature = self._calculate_signature()
        if original_signature != calculated_signature:
            log.warning(
                'Signature mismatch on %s; calculated:"%s" for  %s',
                self._action,
                calculated_signature,
                auth_header,
            )
            self._raise_signature_does_not_match()

    def check_action_permitted(self):
        if (
            self._action == "sts:GetCallerIdentity"
        ):  # always allowed, even if there's an explicit Deny for it
            return True
        policies = self._access_key.collect_policies()
        if not policies:
            log.debug(
                "Without policies on _access_key[%s], action_permitted will not end well",
                self._access_key.arn,
            )
        permitted = False
        for policy in policies:
            iam_policy = IAMPolicy(policy)
            permission_result = iam_policy.is_action_permitted(self._action)
            if permission_result == PermissionResult.DENIED:
                self._raise_access_denied()
            elif permission_result == PermissionResult.PERMITTED:
                permitted = True

        if not permitted:
            log.debug(
                "check_action_permitted(action=%s):DENIED: self=%s policies=%s",
                self._action,
                self,
                policies,
            )
            self._raise_access_denied()

    @abstractmethod
    def _raise_signature_does_not_match(self):
        raise NotImplementedError()

    @abstractmethod
    def _raise_access_denied(self):
        raise NotImplementedError()

    @abstractmethod
    def _raise_invalid_access_key(self, reason: str):
        raise NotImplementedError()

    @abstractmethod
    def _create_auth(self, credentials: Credentials):
        raise NotImplementedError()

    @staticmethod
    def _create_headers_for_aws_request(
        signed_headers: t.List[str], original_headers: t.Dict[str, t.Any]
    ):
        """
        Returns only the headers that were declared in the ``SignedHeaders`` list
        """
        headers = {}
        for key, value in original_headers.items():
            if key.lower() in signed_headers:
                headers[key] = value
        return headers

    def _create_aws_request(self):
        auth_header = self._headers["Authorization"]
        ma = ACCESS_KEY_ID_REGEX.match(auth_header)
        if not ma:
            log.error("Malformed Authorization header: %s", auth_header)
            self._raise_invalid_access_key("Malformed Authorization header")
        signed_headers = ma.group("signed_headers").split(";")
        headers = self._create_headers_for_aws_request(signed_headers, self._headers)
        request = AWSRequest(
            method=self._method, url=self._path, data=self._data, headers=headers
        )
        # this context item is actually used by the signature process
        # and thus should not be removed
        request.context["timestamp"] = headers["X-Amz-Date"]

        return request

    def _calculate_signature(self):
        credentials = self._access_key.create_credentials()
        auth = self._create_auth(credentials)
        request = self._create_aws_request()
        canonical_request = auth.canonical_request(request)
        string_to_sign = auth.string_to_sign(request, canonical_request)
        return auth.signature(string_to_sign, request)

    def __str__(self):
        show = (
            "_data",
            "_action",
            "_access_key",
            "_service",
            "_headers",
            "account_id",
            "_method",
            "_path",
            "_region",
        )
        return repr({k: getattr(self, k) for k in show})

    def __repr__(self):
        show = (
            "_data",
            "_action",
            "_access_key",
            "_service",
            "_headers",
            "account_id",
            "_method",
            "_path",
            "_region",
        )
        return "%s{%s}" % (
            self.__class__.__name__,
            repr({k: getattr(self, k) for k in show}),
        )


class IAMRequest(IAMRequestBase):
    def _raise_signature_does_not_match(self):
        if self._service == "ec2":
            raise AuthFailureError()
        else:
            raise SignatureDoesNotMatchError()

    def _raise_invalid_access_key(self, _):
        if self._service == "ec2":
            raise AuthFailureError()
        else:
            raise InvalidClientTokenIdError()

    def _create_auth(self, credentials: Credentials):
        return SigV4Auth(credentials, self._service, self._region)

    def _raise_access_denied(self):
        raise AccessDeniedError(user_arn=self._access_key.arn, action=self._action)


class S3IAMRequest(IAMRequestBase):
    def _raise_signature_does_not_match(self):
        if "BucketName" in self._data:
            raise BucketSignatureDoesNotMatchError(bucket=self._data["BucketName"])
        else:
            raise S3SignatureDoesNotMatchError()

    def _raise_invalid_access_key(self, reason):
        if reason == "InvalidToken":
            if "BucketName" in self._data:
                raise BucketInvalidTokenError(bucket=self._data["BucketName"])
            else:
                raise S3InvalidTokenError()
        else:
            if "BucketName" in self._data:
                raise BucketInvalidAccessKeyIdError(bucket=self._data["BucketName"])
            else:
                raise S3InvalidAccessKeyIdError()

    def _create_auth(self, credentials: Credentials):
        return S3SigV4Auth(credentials, self._service, self._region)

    def _raise_access_denied(self):
        if "BucketName" in self._data:
            raise BucketAccessDeniedError(bucket=self._data["BucketName"])
        else:
            raise S3AccessDeniedError()


class IAMPolicy(object):
    # the Policy document that will be used as if it was attached
    # to any :root access key
    # language=JSON
    ROOT_CREDENTIAL_POLICY = """
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": "*",
          "Effect": "Allow",
          "Resource": "*"
        }
      ]
    }
    """

    def __init__(self, policy: t.Union[Policy, str, t.Dict[str, t.Any]]):
        if isinstance(policy, Policy):
            default_version = next(
                policy_version
                for policy_version in policy.versions
                if policy_version.is_default
            )
            policy_document = default_version.document
        elif isinstance(policy, str):
            policy_document = policy
        else:
            policy_document = policy["policy_document"]

        self._policy_json = json.loads(policy_document)

    def is_action_permitted(self, action):
        permitted = False
        if isinstance(self._policy_json["Statement"], list):
            for policy_statement in self._policy_json["Statement"]:
                iam_policy_statement = IAMPolicyStatement(policy_statement)
                permission_result = iam_policy_statement.is_action_permitted(action)
                if permission_result == PermissionResult.DENIED:
                    return permission_result
                elif permission_result == PermissionResult.PERMITTED:
                    permitted = True
        else:  # dict
            iam_policy_statement = IAMPolicyStatement(self._policy_json["Statement"])
            return iam_policy_statement.is_action_permitted(action)

        if permitted:
            return PermissionResult.PERMITTED
        else:
            return PermissionResult.NEUTRAL


class IAMPolicyStatement(object):
    def __init__(self, statement: t.Dict[str, t.Any]):
        self._statement = statement

    def is_action_permitted(self, action):
        is_action_concerned = False

        if "NotAction" in self._statement:
            if not self._check_element_matches("NotAction", action):
                is_action_concerned = True
        elif "Action" in self._statement:
            if self._check_element_matches("Action", action):
                is_action_concerned = True
        else:
            log.error("Malformed PolicyStatement: %s", repr(self._statement))
            raise TypeError("Malformed 'Statement'")

        if is_action_concerned:
            if self._statement["Effect"] == "Allow":
                return PermissionResult.PERMITTED
            else:  # Deny
                return PermissionResult.DENIED
        else:
            return PermissionResult.NEUTRAL

    def _check_element_matches(self, statement_element: str, value):
        element = self._statement[statement_element]
        if isinstance(element, list):
            for statement_element_value in element:
                if self._match(statement_element_value, value):
                    return True
            return False
        elif isinstance(element, str):
            return self._match(element, value)
        else:
            log.error(
                "Malformed PolicyStatement[%s]: %s",
                statement_element,
                repr(self._statement),
            )
            raise TypeError("Malformed 'Statement' element")

    @staticmethod
    def _match(pattern, string):
        pattern = pattern.replace("*", ".*")
        pattern = "^{pattern}$".format(pattern=pattern)
        return re.match(pattern, string)


class PermissionResult(Enum):
    PERMITTED = 1
    DENIED = 2
    NEUTRAL = 3
