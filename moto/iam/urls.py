from .responses import IamResponse

# language=RegExp
url_bases = [r"https?://iam\.(.*\.)?amazonaws\.com"]

# language=RegExp
url_paths = {"/$": IamResponse.dispatch}
