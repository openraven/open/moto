from .responses import LogsResponse

# language=RegExp
url_bases = [r"https?://logs\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": LogsResponse.dispatch}
