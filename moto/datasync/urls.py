from .responses import DataSyncResponse

# language=RegExp
url_bases = [r"https?://(.*\.)?(datasync)\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {"/$": DataSyncResponse.dispatch}
