from .responses import StepFunctionResponse

# language=RegExp
url_bases = [r"https?://states\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {"/$": StepFunctionResponse.dispatch}
