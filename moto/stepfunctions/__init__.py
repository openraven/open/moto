from .models import stepfunctions_backends
from ..core.models import base_decorator

stepfunctions_backend = stepfunctions_backends["us-east-1"]
mock_stepfunctions = base_decorator(stepfunctions_backends)
