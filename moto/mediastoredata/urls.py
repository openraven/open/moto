from .responses import MediaStoreDataResponse

# language=RegExp
url_bases = [
    r"https?://data.mediastore\.(.+)\.amazonaws.com",
]

response = MediaStoreDataResponse()

# language=RegExp
url_paths = {"/$": response.dispatch, "/(?P<Path>[^/.]+)$": response.dispatch}
