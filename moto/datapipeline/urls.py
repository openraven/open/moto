from .responses import DataPipelineResponse

# language=RegExp
url_bases = [r"https?://datapipeline\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": DataPipelineResponse.dispatch}
