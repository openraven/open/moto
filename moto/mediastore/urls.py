from .responses import MediaStoreResponse

# language=RegExp
url_bases = [r"https?://mediastore\.(.+)\.amazonaws\.com"]

response = MediaStoreResponse()

# language=RegExp
url_paths = {"/$": response.dispatch, "/(?P<Path>[^/.]+)$": response.dispatch}
