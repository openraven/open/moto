from .responses import ManagedBlockchainResponse

# language=RegExp
url_bases = [r"https?://managedblockchain\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {
    "/networks$": ManagedBlockchainResponse.network_response,
    "/networks/(?P<networkid>[^/.]+)$": ManagedBlockchainResponse.networkid_response,
    "/networks/(?P<networkid>[^/.]+)/proposals$": ManagedBlockchainResponse.proposal_response,
    "/networks/(?P<networkid>[^/.]+)/proposals/(?P<proposalid>[^/.]+)$": ManagedBlockchainResponse.proposalid_response,
    "/networks/(?P<networkid>[^/.]+)/proposals/(?P<proposalid>[^/.]+)/votes$": ManagedBlockchainResponse.proposal_votes_response,
    "/invitations$": ManagedBlockchainResponse.invitation_response,
    "/invitations/(?P<invitationid>[^/.]+)$": ManagedBlockchainResponse.invitationid_response,
    "/networks/(?P<networkid>[^/.]+)/members$": ManagedBlockchainResponse.member_response,
    "/networks/(?P<networkid>[^/.]+)/members/(?P<memberid>[^/.]+)$": ManagedBlockchainResponse.memberid_response,
    "/networks/(?P<networkid>[^/.]+)/members/(?P<memberid>[^/.]+)/nodes$": ManagedBlockchainResponse.node_response,
    "/networks/(?P<networkid>[^/.]+)/members/(?P<memberid>[^/.]+)/nodes?(?P<querys>[^/.]+)$": ManagedBlockchainResponse.node_response,
    "/networks/(?P<networkid>[^/.]+)/members/(?P<memberid>[^/.]+)/nodes/(?P<nodeid>[^/.]+)$": ManagedBlockchainResponse.nodeid_response,
    # >= botocore 1.19.41 (API change - memberId is now part of query-string or body)
    "/networks/(?P<networkid>[^/.]+)/nodes$": ManagedBlockchainResponse.node_response,
    "/networks/(?P<networkid>[^/.]+)/nodes/(?P<nodeid>[^/.]+)$": ManagedBlockchainResponse.nodeid_response,
}
