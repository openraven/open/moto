from .responses import WAFV2Response

url_bases = [
    r"https?://wafv2\.(.+)\.amazonaws.com",
]

url_paths = {
    "/": WAFV2Response.dispatch,
}
