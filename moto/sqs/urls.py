from .responses import SQSResponse

# language=RegExp
url_bases = [r"https?://(.*\.)?(queue|sqs)\.(.*\.)?amazonaws\.com"]

dispatch = SQSResponse().dispatch

# language=RegExp
url_paths = {
    "/$": dispatch,
    r"/(?P<account_id>\d+)/(?P<queue_name>[a-zA-Z0-9\-_.]+)": dispatch,
}
