from .responses import AWSCertificateManagerResponse

# language=RegExp
url_bases = [r"https?://acm\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": AWSCertificateManagerResponse.dispatch}
