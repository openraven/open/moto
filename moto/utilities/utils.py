import base64
import os
import json
import logging
import random
import re
import string
import pkgutil

from boto3 import Session
from collections.abc import MutableMapping

logger = logging.getLogger(__name__)

ACCESS_KEY_ID_REGEX = re.compile(
    r"AWS4-\S+ Credential=(?P<access_key_id>[^/]{1,20})"
    r"/(?P<yyyymmdd>\d{8})/(?P<region>[^/]+)/(?P<service>[^/]+)/[^,]+"
    r", SignedHeaders=(?P<signed_headers>[^,]+), Signature=(?P<signature>.+)$",
)
"""
https://docs.aws.amazon.com/general/latest/gr/sigv4-signed-request-examples.html

>>> auth_header = 'AWS4-HMAC-SHA256 Credential=AKIA0000000000000000/20210729/us-east-1/iam/aws4_request, SignedHeaders=content-type;host;x-amz-date, Signature=b07da49c2a772c5fc232700203e681d6c1a09e7317893654ce5052cf449c2d57'
>>> ma = ACCESS_KEY_ID_REGEX.match(auth_header)
>>> ma.groupdict()
{
  "access_key_id": "AKIA0000000000000000",
  "yyyymmdd": "20210729",
  "aws_region": "us-east-1",
  "service": "iam",
  "signed_headers": "content-type;host;x-amz-date",
  "signature": "b07da49c2a772c5fc232700203e681d6c1a09e7317893654ce5052cf449c2d57"
}

"""


def str2bool(v):
    if v in ("yes", True, "true", "True", "TRUE", "t", "1"):
        return True
    elif v in ("no", False, "false", "False", "FALSE", "f", "0"):
        return False


def random_string(length=None):
    n = length or 20
    random_str = "".join(
        [random.choice(string.ascii_letters + string.digits) for _ in range(n)]
    )
    return random_str


def load_resource(package, resource, as_json=True):
    """
    Open a file, and return the contents as JSON.
    Usage:
    load_resource(__name__, "resources/file.json")
    """
    resource = pkgutil.get_data(package, resource)
    return json.loads(resource) if as_json else resource.decode("utf-8")


def merge_multiple_dicts(*args):
    result = {}
    for d in args:
        result.update(d)
    return result


def filter_resources(resources, filters, attr_pairs):
    """
    Used to filter resources. Usually in get and describe apis.
    """
    result = resources.copy()
    for resource in resources:
        for attrs in attr_pairs:
            values = filters.get(attrs[0]) or None
            if values:
                instance = getattr(resource, attrs[1])
                if (len(attrs) <= 2 and instance not in values) or (
                    len(attrs) == 3 and instance.get(attrs[2]) not in values
                ):
                    result.remove(resource)
                    break
    return result


class LowercaseDict(MutableMapping):
    """A dictionary that lowercases all keys"""

    def __init__(self, *args, **kwargs):
        self.store = dict()
        self.update(dict(*args, **kwargs))  # use the free update to set keys

    def __getitem__(self, key):
        return self.store[self._keytransform(key)]

    def __setitem__(self, key, value):
        self.store[self._keytransform(key)] = value

    def __delitem__(self, key):
        del self.store[self._keytransform(key)]

    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    def __repr__(self):
        return str(self.store)

    @staticmethod
    def _keytransform(key):
        return key.lower()


def initialize_region_and_partition_backends(
    boto_service_name, backend_cls, only_main_partition=False
):
    """
    Using formal type annotations would allow us to represent the actual relationship
    of the provided class to the types of the resulting Dict

    :param str boto_service_name:
    :param class backend_cls: the subclass of :class:`BaseBackend` that will be instantiated per region
    :param bool only_main_partition: if True, only declare the regions from the ``aws`` partition, omitting gov-cloud and china
    :rtype: typing.Dict[str, typing.Any]
    """
    results = {}
    session = Session()
    service_regions = session.get_available_regions(boto_service_name)
    if not service_regions:
        # it seems at the least "support" does this, maybe others
        logger.warning(
            'It seems boto service "%s" has no regions' '; defaulting to "us-east-1"',
            boto_service_name,
        )
        service_regions = ["us-east-1"]
    for region in service_regions:
        try:
            backend = backend_cls(region)
        except TypeError:
            logger.exception("Unable to instantiate %s", backend_cls, exc_info=True)
            raise
        results[region] = backend
    if not only_main_partition:
        for region in session.get_available_regions(
            boto_service_name, partition_name="aws-us-gov"
        ):
            results[region] = backend_cls(region)
        for region in session.get_available_regions(
            boto_service_name, partition_name="aws-cn"
        ):
            results[region] = backend_cls(region)
    return results


def random_alphanumeric(length):
    return "".join(
        str(random.choice(string.ascii_letters + string.digits + "+" + "/"))
        for _ in range(length)
    )


def random_resource_id(size=20):
    chars = list(range(10)) + list(string.ascii_lowercase)

    return "".join(str(random.choice(chars)) for _ in range(size))


def random_access_key():
    return "".join(
        str(random.choice(string.ascii_uppercase + string.digits)) for _ in range(16)
    )


def random_policy_id():
    return "A" + "".join(
        random.choice(string.ascii_uppercase + string.digits) for _ in range(20)
    )


def base36_enc(n):
    # apparently the order matters:
    # Similarly, the 32-bit signed integer maximum value of "2147483647" is "ZIK0ZJ" in base-36.
    alphabet = string.digits + string.ascii_uppercase
    x = []
    while n != 0:
        n, i = divmod(n, len(alphabet))
        x.append(alphabet[i])
    return "".join(reversed(x))


def base36_dec(s: str):
    """for clarity only, python allows ``int(s, 36)`` to do the right thing"""
    # apparently the order matters
    alphabet = string.digits + string.ascii_uppercase
    result = 0
    for pos, ch in enumerate(reversed(s)):
        result += alphabet.index(ch) * (36 ** pos)
    return result


def account_id_from_access_key_id(access_key_id, default_id):
    """
    :param str access_key_id:
    :param str default_id: the ID to use if unable to decode the provided one
    :return: the 12 digit account_id as a string
    :rtype: str
    """
    ma = re.search(r"^A[KS]IA(.{8})", access_key_id)
    if not ma:
        logging.warning(
            "Unable to decipher AccountId from AccessKeyId: %s"
            "; you get the default one",
            access_key_id,
        )
        return default_id
    a_num_36 = ma.group(1)
    a_num = int(a_num_36, 36)
    if 0 > a_num or a_num > 9999_9999_9999:
        logging.warning("Skipping non-base36 AccountId: %s", access_key_id)
        return default_id
    return "{:0>12}".format(a_num)


def random_access_key_id_tail(account_id):
    """
    Returns the **non prefixed** access key id material,
    with the expectation the caller will prepend ``AKIA``
    or ``ASIA`` depending on whether it is to be used as an
    Access Key or a Session Token

    >>> len('{:0>8}'.format(base36_enc(9)))
    8
    >>> len('{:0>8}'.format(base36_enc(9999_9999_9999)))
    8

    :param str account_id: the full 12 digit account_id
    :return: an access key suffix that encodes that account_id
    :rtype: str
    """
    a_num = int(account_id)
    a_enc = "{:0>8}".format(base36_enc(a_num))
    result = a_enc + _random_uppercase_or_digit_sequence(8)
    return result


def random_secret_access_key():
    # be aware the 30 is the number of bytes, which encodes to 40 characters of str
    return base64.b64encode(os.urandom(30)).decode()


def random_assumed_role_id(account_id):
    return random_access_key_id_tail(account_id)


def _random_uppercase_or_digit_sequence(length):
    return "".join(
        str(random.choice(string.ascii_uppercase + string.digits))
        for _ in range(length)
    )
