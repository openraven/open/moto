from .responses import KinesisVideoResponse

# language=RegExp
url_bases = [
    r"https?://kinesisvideo\.(.+)\.amazonaws.com",
]


response = KinesisVideoResponse()


# language=RegExp
url_paths = {
    "/createStream$": response.dispatch,
    "/describeStream$": response.dispatch,
    "/deleteStream$": response.dispatch,
    "/listStreams$": response.dispatch,
    "/getDataEndpoint$": response.dispatch,
}
