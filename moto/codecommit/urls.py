from .responses import CodeCommitResponse

# language=RegExp
url_bases = [r"https?://codecommit\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": CodeCommitResponse.dispatch}
