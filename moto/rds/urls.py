from .responses import RDSResponse

# language=RegExp
url_bases = [r"https?://rds(\..+)?.amazonaws.com"]

# language=RegExp
url_paths = {"/$": RDSResponse.dispatch}
