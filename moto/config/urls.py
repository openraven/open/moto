from .responses import ConfigResponse

# language=RegExp
url_bases = [r"https?://config\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": ConfigResponse.dispatch}
