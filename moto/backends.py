import importlib
import moto
import sys
import typing as t


decorators = [
    d
    for d in dir(moto)
    if d.startswith("mock_") and not d.endswith("_deprecated") and not d == "mock_all"
]
decorator_functions = [getattr(moto, f) for f in decorators]
__BACKENDS = {
    f.boto3_name: (f.name, f.backend) for f in decorator_functions
}  # type: t.Dict[str, t.Tuple[str, str]]
# these are the current snowflakes whose module name does not equal its boto3_name
__BACKENDS["moto_api"] = ("core", "moto_api_backends")
__BACKENDS["s3bucket_path"] = ("s3", "s3_backends")

# this one is a snowflake because it does not have
# ``mock_instance_metadata = base_decorator(instance_metadata_backends)`` for some reason
__BACKENDS["instance_metadata"] = ("instance_metadata", "instance_metadata_backends")


def _import_backend(boto3_name):
    """
    :param str boto3_name:
    :rtype: t.Dict[str, BaseBackend]
    """
    if not boto3_name:
        raise TypeError("Not without boto3_name")
    module_name, backends_field = __BACKENDS[boto3_name]
    module = importlib.import_module("moto." + module_name)
    return getattr(module, backends_field)


def backends():
    """
    :return: an iterator of all region to "Backend" instance mappings
    :rtype: t.Iterable[t.Dict[str, BaseBackend]]
    """
    for module_name in __BACKENDS.keys():
        yield _import_backend(module_name)


def unique_backends():
    for module_name in sorted(set(__BACKENDS.keys())):
        yield _import_backend(module_name)


def loaded_backends():
    loaded_modules = sys.modules.keys()
    loaded_modules = [m for m in loaded_modules if m.startswith("moto.")]
    imported_backends = [
        name
        for name, (module_name, _) in __BACKENDS.items()
        if f"moto.{module_name}" in loaded_modules
    ]
    for name in imported_backends:
        yield name, _import_backend(name)


def get_backend(name):
    """
    Returns the mapping from regions (which almost always includes ``global``)
    to the :class:`BaseBackend` that services that region.

    :param str name: the boto3 service name
    :return: the imported BaseBackend dict per region
    :rtype: t.Dict[str, BaseBackend]
    :raises KeyError: if there is no such backend service named that
    """
    if not name:
        raise KeyError('Not without a "name"')
    return _import_backend(name)


def get_model(name, region_name):
    """
    :param str name: backend name
    :param str region_name: as expected
    :rtype: object|None
    """
    for backends_ in backends():
        for region, backend in backends_.items():
            if region == region_name:
                backend_class = backend.__class__
                models = getattr(backend_class, "__models__", None)
                if models is None:
                    raise TypeError(
                        "It seems {!r} has no __models__".format(backend_class)
                    )
                if name in models:
                    return list(getattr(backend, models[name])())


def warm_the_backends():
    import logging
    import time

    # doing this causes ModuleLoader deadlocks with the ec2 module
    """
        from threading import Thread
        # put the Thread in a list so we can mutate the local data structure with the results
        threads = [
            Thread(target=_import_backend, args=(module_name,))
            for module_name in BACKENDS.values()
        ]
        t0 = time.time()
        list(map(lambda it: it.start(), threads))
        list(map(lambda it: it.join(), threads))
        t1 = time.time()
        logging.info('multi-threaded warm process took %s', t1 - t0)
    """
    t0 = time.time()
    list(backends())
    t1 = time.time()
    logging.info("single-threaded warm process took %s", t1 - t0)
