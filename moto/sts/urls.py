from .responses import TokenResponse

# language=RegExp
url_bases = [r"https?://sts\.(.*\.)?amazonaws\.com"]

# language=RegExp
url_paths = {"/$": TokenResponse.dispatch}
