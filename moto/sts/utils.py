import base64
import os

# ACCOUNT_SPECIFIC_ACCESS_KEY_PREFIX = "8NWMTLYQ"
# ACCOUNT_SPECIFIC_ASSUMED_ROLE_ID_PREFIX = "3X42LBCD"
SESSION_TOKEN_PREFIX = "FQoGZXIvYXdzEBYaD"
DEFAULT_STS_SESSION_DURATION = 3600


def random_session_token():
    return (
        SESSION_TOKEN_PREFIX
        + base64.b64encode(os.urandom(266))[len(SESSION_TOKEN_PREFIX) :].decode()
    )
