from base64 import b64decode
import datetime
import logging
import typing as t
import xmltodict

from moto.core import ACCOUNT_ID, BaseBackend, BaseModel
from moto.core.utils import iso_8601_datetime_with_milliseconds
from moto.iam import iam_backends_by_account_id
from moto.iam.models import User
from moto.utilities.utils import (
    account_id_from_access_key_id,
    random_access_key_id_tail,
    random_secret_access_key,
    random_assumed_role_id,
)
from .exceptions import MalformedInputError
from .utils import (
    random_session_token,
    DEFAULT_STS_SESSION_DURATION,
)

logger = logging.getLogger(__name__)


class Token(BaseModel):
    """Used by both :method:`STSBackend.get_session_token`
    and :method:`STSBackend.get_federation_token`
    which is why it has so many optional kwargs"""

    user: t.Optional[User]
    """if present, the original IAM User for whom this Token was generated"""
    assumed_role: t.Optional["AssumedRole"]
    """if present, the original AssumedRole for whom this Token was generated"""

    __slots__ = (
        "is_root",
        "account_id",
        "access_key_id",
        "secret_access_key",
        "session_token",
        "expiration",
        "serial_number",
        "token_code",
        "name",
        "policy",
    )

    def __init__(
        self,
        account_id,
        access_key_id,
        duration,
        serial_number=None,
        token_code=None,
        name=None,
        policy=None,
    ):
        self.is_root = False
        self.account_id = account_id
        if duration < 900 or duration > 129600:
            raise MalformedInputError()
        now = datetime.datetime.utcnow()
        self.access_key_id = access_key_id
        self.secret_access_key = random_secret_access_key()
        self.session_token = random_session_token()
        self.expiration = now + datetime.timedelta(seconds=duration)
        self.serial_number = serial_number
        self.token_code = token_code
        self.name = name
        self.policy = policy

    @property
    def expiration_iso8601(self):
        return iso_8601_datetime_with_milliseconds(self.expiration)

    def __str__(self):
        return repr({k: getattr(self, k) for k in self.__slots__})


class AssumedRole(BaseModel):
    __slots__ = (
        "account_id",
        "session_name",
        "role_arn",
        "policy",
        "expiration",
        "external_id",
        "access_key_id",
        "secret_access_key",
        "session_token",
        "assumed_role_id",
    )

    def __init__(
        self, role_session_name, role_arn, policy, duration, external_id, account_id
    ):
        self.account_id = account_id
        self.session_name = role_session_name
        self.role_arn = role_arn
        self.policy = policy
        now = datetime.datetime.utcnow()
        self.expiration = now + datetime.timedelta(seconds=duration)
        self.external_id = external_id
        self.access_key_id = "ASIA" + random_access_key_id_tail(account_id)
        self.secret_access_key = random_secret_access_key()
        self.session_token = random_session_token()
        self.assumed_role_id = "AROA" + random_assumed_role_id(account_id)

    @property
    def expiration_iso8601(self):
        return iso_8601_datetime_with_milliseconds(self.expiration)

    @property
    def user_id(self):
        return self.assumed_role_id + ":" + self.session_name

    @property
    def arn(self):
        return (
            "arn:aws:sts::{account_id}:assumed-role/{role_name}/{session_name}".format(
                account_id=self.account_id,
                role_name=self.role_arn.split("/")[-1],
                session_name=self.session_name,
            )
        )

    def __str__(self):
        return self.arn

    def __repr__(self):
        return "%s(%r)" % (
            self.__class__.__name__,
            {k: getattr(self, k) for k in self.__slots__},
        )


class STSBackend(BaseBackend):
    account_id: t.Optional[str] = None

    def __init__(self):
        self.assumed_roles: t.List[AssumedRole] = []
        self.session_tokens: t.Dict[str, Token] = {}
        """Token.access_key_id -> Token"""

    @staticmethod
    def default_vpc_endpoint_service(service_region, zones):
        """Default VPC endpoint service."""
        return BaseBackend.default_vpc_endpoint_service_factory(
            service_region, zones, "sts"
        )

    def get_session_token(self, access_key_id: str, duration: int):
        """
        :param str access_key_id: the **request** AccessKeyId
        :param int duration: duration in seconds
        """
        account_id = account_id_from_access_key_id(access_key_id, self.account_id)
        is_root = False
        user = None
        assumed_role = None
        if access_key_id.startswith("AKIA"):
            iam_backend = iam_backends_by_account_id(account_id)
            user = iam_backend.get_user_from_access_key_id(access_key_id)
            if not user:
                is_root = iam_backend.is_root_access_key(access_key_id)
        elif access_key_id.startswith("ASIA"):
            assumed_role = self.get_assumed_role_from_access_key(access_key_id)
        session_access_key_id = "ASIA" + random_access_key_id_tail(account_id)
        token = Token(
            account_id,
            session_access_key_id,
            duration=duration,
        )
        token.is_root = is_root
        token.user = user
        token.assumed_role = assumed_role
        self.session_tokens[token.access_key_id] = token
        return token

    def get_federation_token(
        self, access_key_id: str, name: str, duration: int, policy
    ):
        account_id = account_id_from_access_key_id(access_key_id, self.account_id)
        session_access_key_id = "ASIA" + random_access_key_id_tail(account_id)
        token = Token(
            account_id,
            session_access_key_id,
            duration=duration,
            name=name,
            policy=policy,
        )
        return token

    def assume_role(self, **kwargs) -> AssumedRole:
        if t.TYPE_CHECKING:
            from moto.core.responses import BaseResponse
        sts_response: "BaseResponse" = kwargs.pop("response")

        access_key = sts_response.get_current_user()
        account_id = account_id_from_access_key_id(access_key, ACCOUNT_ID)
        my_iam_backend = iam_backends_by_account_id(account_id)

        a_r = self.get_assumed_role_from_access_key(access_key)
        u = None
        if not a_r:
            if my_iam_backend.is_root_access_key(access_key):
                u = my_iam_backend.root_arn
            else:
                u = my_iam_backend.get_user_from_access_key_id(access_key)
                if u:
                    u = u.arn

        want_role_arn = kwargs["role_arn"]
        ma = my_iam_backend.role_arn_regex.match(want_role_arn)
        if ma is None:
            raise Exception("what is that nonsense: " + want_role_arn)

        role_account_id = ma.group("account_id")
        role_name = ma.group("role_name")
        del ma, want_role_arn

        their_iam_backend = iam_backends_by_account_id(role_account_id)
        # just poke it, as the method raises on missing
        r = their_iam_backend.get_role(role_name)
        kwargs["account_id"] = r.account_id
        del role_name

        c_a_r_kwargs = dict(**kwargs)
        c_a_r_kwargs.update({"user": u, "assumed_role": a_r})
        if not r.can_assume_role(**c_a_r_kwargs):
            raise Exception("AssumeRole denied by Policy evaluation")
        del r
        role = AssumedRole(**kwargs)
        self.assumed_roles.append(role)
        return role

    def get_assumed_role_from_access_key(
        self, access_key_id
    ) -> t.Optional[AssumedRole]:
        for assumed_role in self.assumed_roles:
            if assumed_role.access_key_id == access_key_id:
                return assumed_role
        return None

    def assume_role_with_web_identity(self, **kwargs):
        return self.assume_role(**kwargs)

    def assume_role_with_saml(self, **kwargs):
        del kwargs["principal_arn"]
        saml_assertion_encoded = kwargs.pop("saml_assertion")
        saml_assertion_decoded = b64decode(saml_assertion_encoded)

        namespaces = {
            "urn:oasis:names:tc:SAML:2.0:protocol": "samlp",
            "urn:oasis:names:tc:SAML:2.0:assertion": "saml",
        }
        saml_assertion = xmltodict.parse(
            saml_assertion_decoded.decode("utf-8"),
            force_cdata=True,
            process_namespaces=True,
            namespaces=namespaces,
        )

        saml_assertion_attributes = saml_assertion["samlp:Response"]["saml:Assertion"][
            "saml:AttributeStatement"
        ]["saml:Attribute"]
        for attribute in saml_assertion_attributes:
            if (
                attribute["@Name"]
                == "https://aws.amazon.com/SAML/Attributes/RoleSessionName"
            ):
                kwargs["role_session_name"] = attribute["saml:AttributeValue"]["#text"]
            if (
                attribute["@Name"]
                == "https://aws.amazon.com/SAML/Attributes/SessionDuration"
            ):
                kwargs["duration"] = int(attribute["saml:AttributeValue"]["#text"])

        if "duration" not in kwargs:
            kwargs["duration"] = DEFAULT_STS_SESSION_DURATION

        kwargs["external_id"] = None
        kwargs["policy"] = None
        role = AssumedRole(**kwargs)
        self.assumed_roles.append(role)
        return role

    def get_caller_identity(self):
        # Logic resides in responses.py
        # Fake method here to make implementation coverage script aware that this method is implemented
        pass


sts_backend = STSBackend()
