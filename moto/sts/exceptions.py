from moto.core.exceptions import RESTError


class InvalidClientTokenId(RESTError):
    """Raised in the case of an unknown AccessKeyId provided to GetCallerIdentity"""

    code = 403

    def __init__(self):
        super(InvalidClientTokenId, self).__init__(
            "InvalidClientTokenId",
            message="The security token included in the request is invalid.",
            error_type_type="Sender",
        )


class MalformedInputError(RESTError):
    """This does not show up anywhere in ``botocore/data``
    but is returned if ``aws sts get-session-token --duration 99999999``
    Theirs does not produce any explanatory text :-(

    .. xml:

        <ErrorResponse xmlns="https://sts.amazonaws.com/doc/2011-06-15/">
          <Error>
            <Type>Sender</Type>
            <Code>MalformedInput</Code>
          </Error>
          <RequestId>c5287160-f1be-43c4-ae0d-88eab767dcb4</RequestId>
        </ErrorResponse>
    """

    code = 400

    def __init__(self):
        super(MalformedInputError, self).__init__(
            "MalformedInput",
            message="",
            error_type_type="Sender",
            # it doesn't actually need this
            # xmlns="https://sts.amazonaws.com/doc/2011-06-15/"
        )


class STSClientError(RESTError):
    code = 400


class STSValidationError(STSClientError):
    def __init__(self, *args, **kwargs):
        super(STSValidationError, self).__init__("ValidationError", *args, **kwargs)
