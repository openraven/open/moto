from .responses import EmailResponse

# language=RegExp
url_bases = [
    r"https?://email\.(.+)\.amazonaws\.com",
    r"https?://ses\.(.+)\.amazonaws\.com",
]

# language=RegExp
url_paths = {"/$": EmailResponse.dispatch}
