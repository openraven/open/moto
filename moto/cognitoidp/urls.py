from .responses import CognitoIdpResponse, CognitoIdpJsonWebKeyResponse

# language=RegExp
url_bases = [r"https?://cognito-idp\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {
    "/$": CognitoIdpResponse.dispatch,
    "/(?P<user_pool_id>[^/]+)/.well-known/jwks.json$": CognitoIdpJsonWebKeyResponse().serve_json_web_key,
}
