from .responses import IoTDataPlaneResponse

# language=RegExp
url_bases = [r"https?://data\.iot\.(.+)\.amazonaws.com"]


response = IoTDataPlaneResponse()


# language=RegExp
url_paths = {
    #
    # Paths for :class:`moto.core.models.MockAWS`
    #
    # This route requires special handling.
    "/topics/(?P<topic>.*)$": response.dispatch_publish,
    # The remaining routes can be handled by the default dispatcher.
    "/.*$": response.dispatch,
    #
    # (Flask) Paths for :class:`moto.core.models.ServerModeMockAWS`
    #
    # This route requires special handling.
    "/topics/<path:topic>$": response.dispatch_publish,
    # The remaining routes can be handled by the default dispatcher.
    "/<path:route>$": response.dispatch,
}
