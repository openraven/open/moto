from .responses import BatchResponse

# language=RegExp
url_bases = [r"https?://batch\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {
    "/v1/createcomputeenvironment$": BatchResponse.dispatch,
    "/v1/describecomputeenvironments$": BatchResponse.dispatch,
    "/v1/deletecomputeenvironment": BatchResponse.dispatch,
    "/v1/updatecomputeenvironment": BatchResponse.dispatch,
    "/v1/createjobqueue": BatchResponse.dispatch,
    "/v1/describejobqueues": BatchResponse.dispatch,
    "/v1/updatejobqueue": BatchResponse.dispatch,
    "/v1/deletejobqueue": BatchResponse.dispatch,
    "/v1/registerjobdefinition": BatchResponse.dispatch,
    "/v1/deregisterjobdefinition": BatchResponse.dispatch,
    "/v1/describejobdefinitions": BatchResponse.dispatch,
    "/v1/submitjob": BatchResponse.dispatch,
    "/v1/describejobs": BatchResponse.dispatch,
    "/v1/listjobs": BatchResponse.dispatch,
    "/v1/terminatejob": BatchResponse.dispatch,
    "/v1/canceljob": BatchResponse.dispatch,
}
