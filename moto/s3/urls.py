from moto import settings

from .responses import S3ResponseInstance

# language=RegExp
url_bases = [
    r"https?://s3(.*)\.amazonaws.com",
    r"https?://(?P<bucket_name>[a-zA-Z0-9\-_.]*)\.?s3(.*)\.amazonaws.com",
]

url_bases.extend(settings.get_s3_custom_endpoints())

# language=RegExp
url_paths = {
    # subdomain bucket
    "/$": S3ResponseInstance.bucket_response,
    # subdomain key of path-based bucket
    "/(?P<key_or_bucket_name>[^/]+)/?$": S3ResponseInstance.ambiguous_response,
    # path-based bucket + key
    "/(?P<bucket_name_path>[^/]+)/(?P<key_name>.+)": S3ResponseInstance.key_or_control_response,
    # subdomain bucket + key with empty first part of path
    "/(?P<key_name>/.*)$": S3ResponseInstance.key_or_control_response,
}
