from .responses import ElasticMapReduceResponse

# language=RegExp
url_bases = [
    r"https?://(.+)\.elasticmapreduce\.amazonaws.com",
    r"https?://elasticmapreduce\.(.+)\.amazonaws.com",
]

# language=RegExp
url_paths = {"/$": ElasticMapReduceResponse.dispatch}
