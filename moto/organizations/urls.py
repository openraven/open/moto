from .responses import OrganizationsResponse

# language=RegExp
url_bases = [r"https?://organizations\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": OrganizationsResponse.dispatch}
