from .models import org_backend_by_account_id
from ..core.models import ACCOUNT_ID, base_decorator

organizations_backends = {"global": org_backend_by_account_id(ACCOUNT_ID)}
mock_organizations = base_decorator(organizations_backends)
