from .responses import ECRResponse

# language=RegExp
url_bases = [
    r"https?://ecr\.(.+)\.amazonaws\.com",
    r"https?://api\.ecr\.(.+)\.amazonaws\.com",
]

# language=RegExp
url_paths = {"/$": ECRResponse.dispatch}
