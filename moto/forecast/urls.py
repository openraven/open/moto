from .responses import ForecastResponse

# language=RegExp
url_bases = [r"https?://forecast\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": ForecastResponse.dispatch}
