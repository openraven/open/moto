from ..elb.urls import api_version_elb_backend

# language=RegExp
url_bases = [r"https?://elasticloadbalancing\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {"/$": api_version_elb_backend}
