from .responses import MediaLiveResponse

# language=RegExp
url_bases = [
    r"https?://medialive\.(.+)\.amazonaws.com",
]


response = MediaLiveResponse()


# language=RegExp
url_paths = {
    "/prod/channels": response.dispatch,
    "/prod/channels/(?P<channelid>[^/.]+)": response.dispatch,
    "/prod/channels/(?P<channelid>[^/.]+)/start": response.dispatch,
    "/prod/channels/(?P<channelid>[^/.]+)/stop": response.dispatch,
    "/prod/inputs": response.dispatch,
    "/prod/inputs/(?P<inputid>[^/.]+)": response.dispatch,
}
