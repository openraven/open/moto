from .responses import DynamoHandler

# language=RegExp
url_bases = [r"https?://dynamodb\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/": DynamoHandler.dispatch}
