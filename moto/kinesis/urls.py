from .responses import KinesisResponse

# language=RegExp
url_bases = [
    # Need to avoid conflicting with kinesisvideo
    r"https?://kinesis\.(.+)\.amazonaws\.com",
]

# language=RegExp
url_paths = {"/$": KinesisResponse.dispatch}
