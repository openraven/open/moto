from .responses import SimpleSystemManagerResponse

# language=RegExp
url_bases = [
    r"https?://ssm\.(.+)\.amazonaws\.com",
    r"https?://ssm\.(.+)\.amazonaws\.com\.cn",
]

# language=RegExp
url_paths = {"/$": SimpleSystemManagerResponse.dispatch}
