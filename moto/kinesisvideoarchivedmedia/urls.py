from .responses import KinesisVideoArchivedMediaResponse

# language=RegExp
url_bases = [
    r"https?://.*\.kinesisvideo\.(.+)\.amazonaws.com",
]


response = KinesisVideoArchivedMediaResponse()


# language=RegExp
url_paths = {
    "/.*$": response.dispatch,
}
