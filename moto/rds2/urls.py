from .responses import RDS2Response

# language=RegExp
url_bases = [r"https?://rds\.(.+)\.amazonaws\.com", r"https?://rds\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": RDS2Response.dispatch}
