from .responses import ApplicationAutoScalingResponse

# language=RegExp
url_bases = [r"https?://application-autoscaling\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {
    "/$": ApplicationAutoScalingResponse.dispatch,
}
