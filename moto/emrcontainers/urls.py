"""emrcontainers base URL and path."""
from .responses import EMRContainersResponse

# language=RegExp
url_bases = [
    r"https?://emr-containers\.(.+)\.amazonaws\.com",
]


# language=RegExp
url_paths = {
    "/virtualclusters$": EMRContainersResponse.dispatch,
    "/virtualclusters/(?P<virtualClusterId>[^/]+)$": EMRContainersResponse.dispatch,
    "/virtualclusters/(?P<virtualClusterId>[^/]+)/jobruns$": EMRContainersResponse.dispatch,
    "/virtualclusters/(?P<virtualClusterId>[^/]+)/jobruns/(?P<jobRunId>[^/]+)$": EMRContainersResponse.dispatch,
}
