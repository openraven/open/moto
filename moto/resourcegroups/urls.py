from .responses import ResourceGroupsResponse

# language=RegExp
url_bases = [r"https?://resource-groups(-fips)?\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {
    "/delete-group$": ResourceGroupsResponse.dispatch,
    "/get-group$": ResourceGroupsResponse.dispatch,
    "/get-group-configuration$": ResourceGroupsResponse.dispatch,
    "/put-group-configuration$": ResourceGroupsResponse.dispatch,
    "/get-group-query$": ResourceGroupsResponse.dispatch,
    "/groups$": ResourceGroupsResponse.dispatch,
    "/groups/(?P<resource_group_name>[^/]+)$": ResourceGroupsResponse.dispatch,
    "/groups/(?P<resource_group_name>[^/]+)/query$": ResourceGroupsResponse.dispatch,
    "/groups-list$": ResourceGroupsResponse.dispatch,
    "/resources/(?P<resource_arn>[^/]+)/tags$": ResourceGroupsResponse.dispatch,
    "/update-group$": ResourceGroupsResponse.dispatch,
    "/update-group-query$": ResourceGroupsResponse.dispatch,
}
