from .responses import EventsHandler

# language=RegExp
url_bases = [r"https?://events\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/": EventsHandler.dispatch}
