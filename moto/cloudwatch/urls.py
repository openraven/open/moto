from .responses import CloudWatchResponse

# language=RegExp
url_bases = [r"https?://monitoring\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {"/$": CloudWatchResponse.dispatch}
