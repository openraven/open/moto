from .responses import IoTResponse

# language=RegExp
url_bases = [r"https?://iot\.(.+)\.amazonaws\.com"]


response = IoTResponse()


# language=RegExp
url_paths = {
    #
    # Paths for :class:`moto.core.models.MockAWS`
    #
    # This route requires special handling.
    "/attached-policies/(?P<target>.*)$": response.dispatch_attached_policies,
    # The remaining routes can be handled by the default dispatcher.
    "/.*$": response.dispatch,
    #
    # (Flask) Paths for :class:`moto.core.models.ServerModeMockAWS`
    #
    # This route requires special handling.
    "/attached-policies/<path:target>$": response.dispatch_attached_policies,
    # The remaining routes can be handled by the default dispatcher.
    "/<path:route>$": response.dispatch,
}
