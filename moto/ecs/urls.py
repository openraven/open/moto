from .responses import EC2ContainerServiceResponse

# language=RegExp
url_bases = [r"https?://ecs\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": EC2ContainerServiceResponse.dispatch}
