from .models import elasticbeanstalk_backends
from moto.core.models import base_decorator

mock_elasticbeanstalk = base_decorator(elasticbeanstalk_backends)
