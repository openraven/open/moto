from .responses import EBResponse

# language=RegExp
url_bases = [
    r"https?://elasticbeanstalk\.(?P<region>[a-zA-Z0-9\-_]+)\.amazonaws.com",
]

# language=RegExp
url_paths = {
    "/$": EBResponse.dispatch,
}
