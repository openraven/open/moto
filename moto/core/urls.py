from .responses import MotoAPIResponse

# language=RegExp
url_bases = ["https?://motoapi.amazonaws.com"]

response_instance = MotoAPIResponse()

# language=RegExp
url_paths = {
    "/moto-api/$": response_instance.dashboard,
    "/moto-api/data.json": response_instance.model_data,
    "/moto-api/root_access_key": response_instance.generate_root_keys,
    "/moto-api/reset": response_instance.reset_response,
    "/moto-api/reset-auth": response_instance.reset_auth_response,
}
