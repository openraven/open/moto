from .models import awslambda_backends
from ..core.models import base_decorator, deprecated_base_decorator

awslambda_backend = awslambda_backends["us-east-1"]
mock_lambda = base_decorator(awslambda_backends)
mock_lambda_deprecated = deprecated_base_decorator(awslambda_backends)
