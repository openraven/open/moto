from .responses import Ec2InstanceConnectResponse

# language=RegExp
url_bases = [r"https?://ec2-instance-connect\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": Ec2InstanceConnectResponse.dispatch}
