import json
from moto.core import BaseBackend
from ..utilities.utils import initialize_region_and_partition_backends


class Ec2InstanceConnectBackend(BaseBackend):
    def __init__(self, region):
        self.region = region

    @staticmethod
    def send_ssh_public_key():
        return json.dumps(
            {"RequestId": "example-2a47-4c91-9700-e37e85162cb6", "Success": True}
        )


ec2instanceconnect_backends = initialize_region_and_partition_backends(
    "ec2", Ec2InstanceConnectBackend
)
