from .responses import GlueResponse

# language=RegExp
url_bases = [r"https?://glue\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": GlueResponse.dispatch}
