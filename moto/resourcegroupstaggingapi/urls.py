from .responses import ResourceGroupsTaggingAPIResponse

# language=RegExp
url_bases = [r"https?://tagging\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {"/$": ResourceGroupsTaggingAPIResponse.dispatch}
