from .responses import PollyResponse

# language=RegExp
url_bases = [r"https?://polly\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {
    "/v1/voices": PollyResponse.dispatch,
    "/v1/lexicons/(?P<lexicon>[^/]+)": PollyResponse.dispatch,
    "/v1/lexicons": PollyResponse.dispatch,
    "/v1/speech": PollyResponse.dispatch,
}
