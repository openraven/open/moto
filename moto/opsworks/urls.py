from .responses import OpsWorksResponse

# AWS OpsWorks has a single endpoint: opsworks.us-east-1.amazonaws.com
# and only supports HTTPS requests.
# language=RegExp
url_bases = [r"https?://opsworks\.us-east-1\.amazonaws.com"]

# language=RegExp
url_paths = {"/$": OpsWorksResponse.dispatch}
