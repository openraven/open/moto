from moto.dynamodb2.models import dynamodb_backends as dynamodb2_backends
from ..core.models import base_decorator, deprecated_base_decorator

dynamodb2_backend = dynamodb2_backends["us-east-1"]
mock_dynamodb2 = base_decorator(dynamodb2_backends)
mock_dynamodb2_deprecated = deprecated_base_decorator(dynamodb2_backends)
