from .responses import DynamoDBStreamsHandler

# language=RegExp
url_bases = [r"https?://streams\.dynamodb\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {"/$": DynamoDBStreamsHandler.dispatch}
