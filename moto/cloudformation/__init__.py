from .models import _cloudformation_backends as cloudformation_backends
from ..core.models import base_decorator, deprecated_base_decorator

cloudformation_backend_east_1 = cloudformation_backends["us-east-1"]
mock_cloudformation = base_decorator(cloudformation_backends)
mock_cloudformation_deprecated = deprecated_base_decorator(cloudformation_backends)
