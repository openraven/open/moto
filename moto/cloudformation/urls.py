from .responses import CloudFormationResponse

# language=RegExp
url_bases = [r"https?://cloudformation\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {
    "/$": CloudFormationResponse.dispatch,
    "/cloudformation_(?P<region>[^/]+)/cfnresponse$": CloudFormationResponse.cfnresponse,
}
