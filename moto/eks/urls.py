from .responses import EKSResponse

# language=RegExp
url_bases = [
    r"https?://eks\.(.+)\.amazonaws.com",
]


response = EKSResponse()


# language=RegExp
url_paths = {
    "/clusters$": response.dispatch,
    "/clusters/(?P<name>[^/]+)$": response.dispatch,
    "/clusters/(?P<name>[^/]+)/node-groups$": response.dispatch,
    "/clusters/(?P<name>[^/]+)/node-groups/(?P<nodegroupName>[^/]+)$": response.dispatch,
    "/clusters/(?P<name>[^/]+)/fargate-profiles$": response.dispatch,
    "/clusters/(?P<name>[^/]+)/fargate-profiles/(?P<fargateProfileName>[^/]+)$": response.dispatch,
}
