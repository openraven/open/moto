from .responses import SNSResponse

# language=RegExp
url_bases = [r"https?://sns\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": SNSResponse.dispatch}
