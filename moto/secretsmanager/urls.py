from .responses import SecretsManagerResponse

# language=RegExp
url_bases = [r"https?://secretsmanager\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": SecretsManagerResponse.dispatch}
