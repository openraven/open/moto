from .responses import DatabaseMigrationServiceResponse

# language=RegExp
url_bases = [r"https?://dms\.(.+)\.amazonaws\.com"]


# language=RegExp
url_paths = {
    "/$": DatabaseMigrationServiceResponse.dispatch,
}
