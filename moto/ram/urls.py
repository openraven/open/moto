from .responses import ResourceAccessManagerResponse

# language=RegExp
url_bases = [r"https?://ram\.(.+)\.amazonaws.com"]

# language=RegExp
url_paths = {
    "/createresourceshare$": ResourceAccessManagerResponse.dispatch,
    "/deleteresourceshare/?$": ResourceAccessManagerResponse.dispatch,
    "/enablesharingwithawsorganization$": ResourceAccessManagerResponse.dispatch,
    "/getresourceshares$": ResourceAccessManagerResponse.dispatch,
    "/updateresourceshare$": ResourceAccessManagerResponse.dispatch,
}
