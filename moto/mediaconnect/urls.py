from .responses import MediaConnectResponse

# language=RegExp
url_bases = [
    r"https?://mediaconnect\.(.+)\.amazonaws.com",
]


response = MediaConnectResponse()


# language=RegExp
url_paths = {
    "/v1/flows": response.dispatch,
    "/v1/flows/(?P<flowarn>[^/.]+)": response.dispatch,
    "/v1/flows/(?P<flowarn>[^/.]+)/vpcInterfaces": response.dispatch,
    "/v1/flows/(?P<flowarn>[^/.]+)/vpcInterfaces/(?P<vpcinterfacename>[^/.]+)": response.dispatch,
    "/v1/flows/(?P<flowarn>[^/.]+)/outputs": response.dispatch,
    "/v1/flows/(?P<flowarn>[^/.]+)/outputs/(?P<outputarn>[^/.]+)": response.dispatch,
    "/v1/flows/start/(?P<flowarn>[^/.]+)": response.dispatch,
    "/v1/flows/stop/(?P<flowarn>[^/.]+)": response.dispatch,
    "/tags/(?P<resourcearn>[^/.]+)": response.dispatch,
}
