from .responses import EC2Response


# language=RegExp
url_bases = [r"https?://ec2\.(.+)\.amazonaws\.com(|\.cn)"]

# language=RegExp
url_paths = {"/": EC2Response.dispatch}
