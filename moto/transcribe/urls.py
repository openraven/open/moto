from .responses import TranscribeResponse

# language=RegExp
url_bases = [r"https?://transcribe\.(.+)\.amazonaws\.com"]

# language=RegExp
url_paths = {"/$": TranscribeResponse.dispatch}
